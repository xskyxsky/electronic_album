namespace TsingPigSDK
{
    public class UIType
    {
        public string Name { get; private set; }

        public UIType(string name)
        {
            Name = name;
        }
    }
}
using MVPFrameWork;

public interface IPhotoDetailPresenter : IPresenter
{
    void Quit();

    void DeletePhoto();
}
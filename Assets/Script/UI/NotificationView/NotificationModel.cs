public class NotificationModel : INotificationModel
{
    private string _title;

    public string Title { get => _title; set => _title = value; }
}
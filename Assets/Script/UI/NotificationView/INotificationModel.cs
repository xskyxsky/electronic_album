public interface INotificationModel : IModel
{
    string Title { get; set; }
}
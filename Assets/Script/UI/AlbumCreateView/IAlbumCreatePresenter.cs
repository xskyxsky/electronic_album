using MVPFrameWork;

public interface IAlbumCreatePresenter : IPresenter
{
    void Quit();

    void CreateAlbum();
}
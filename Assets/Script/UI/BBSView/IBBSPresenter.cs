using MVPFrameWork;

public interface IBBSPresenter : IPresenter
{
    void TryDeleteSection();

    void EnterCreatePostItemView();
}
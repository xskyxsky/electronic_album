public static class StrDef
{
    public const string CANVAS = "CanvasRoot/Canvas";
    public const string CANVAS_1 = "CanvasRoot/Canvas_1";
    public const string CANVAS_2 = "CanvasRoot/Canvas_2";
    public const string ALBUM_ITEM_DATA_PATH = "AlbumItem";
    public const string PHOTO_ITEM_DATA_PATH = "PhotoItem";
    public const string PHOTO_UPLOAD_ITEM_DATA_PATH = "PhotoUploadItem";
    public const string MOMENT_PHOTO_ITEM_DATA_PATH = "MomentPhotoItem";
    public const string PHOTO_WALL_ITEM_DATA_PATH = "PhotoWallItem";
    public const string B_B_S_TYPE_ITEM_DATA_PATH = "BBSTypeItem";
    public const string B_B_S_POST_ITEM_DATA_PATH = "BBSPostItem";
    public const string B_B_S_POST_PHOTO_ITEM_DATA_PATH = "BBSPostPhotoItem";
    public const string POST_ITEM_DATA_PATH = "PostItem";
    public const string POST_PHOTO_ITEM_DATA_PATH = "PostPhotoItem";
    public const string COMMENT_ITEM_DATA_PATH = "CommentItem";
}
public static class ViewId
{
    public const int LoginView = 1000;
    public const int MainView = 1001;
    public const int AlbumCreateView = 1002;
    public const int PhotoView = 1003;
    public const int PhotoDetailView = 1004;
    public const int CreatePhotoWallItemView = 1005;
    public const int MomentView = 1007;
    public const int BBSTypeCreateView = 1008;
    public const int BBSView = 1009;
    public const int CreatePostItemView = 1010;
    public const int PostView = 1011;
    public const int NotificationView = 1012;
}